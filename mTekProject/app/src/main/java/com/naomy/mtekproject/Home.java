package com.naomy.mtekproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Home extends AppCompatActivity {

    Button logout,insurance,claims,appointments,reports,biling,help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initElem();
    }

    public void initElem() {
        logout = (Button)findViewById(R.id.Btn_logout);
        insurance = (Button)findViewById(R.id.Btn_insurance);
        claims = (Button)findViewById(R.id.Btn_claims);
        appointments = (Button)findViewById(R.id.Btn_apptnmnts);
        reports = (Button)findViewById(R.id.Btn_reports);
        biling = (Button)findViewById(R.id.Btn_biling);
        help = (Button)findViewById(R.id.Btn_help);


    }

    public void onClickHome(View v){
        switch (v.getId()){
            case R.id.Btn_insurance:
                showMessage("Showing insurance details");
                break;
            case R.id.Btn_claims:
                showMessage("Showing claims");
                break;
            case R.id.Btn_apptnmnts:
                showMessage("Showing appointments details");
                break;
            case R.id.Btn_reports:
                showMessage("Showing reports");
                break;
            case R.id.Btn_help:
                showMessage("Showing help options");
                break;
            case R.id.Btn_biling:
                showMessage("Showing your bills");
                break;
            case R.id.Btn_logout:
                showMessage("Logging out");
                Intent i = new Intent(Home.this,Logscreen.class);
                startActivity(i);
                break;

            default:
                showMessage("Nothing selected");
                break;
        }
    }

    public void showMessage(String text){
        Toast.makeText(Home.this,text,Toast.LENGTH_SHORT).show();
    }
}
