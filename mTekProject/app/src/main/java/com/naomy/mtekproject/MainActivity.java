package com.naomy.mtekproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    ImageView imageMtek;
    ProgressBar pBar;
    Button next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageMtek = (ImageView) findViewById(R.id.ImageMtek);
        pBar = (ProgressBar) findViewById(R.id.progressMain);
        next = (Button)findViewById(R.id.ProceedBtn);


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,Logscreen.class);
                startActivity(i);
            }
        });

    }
}
